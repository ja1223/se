const uu6 = require('uu6')
const mm6 = require('../src')
const {T} = mm6

let shape = [2,2,3], idx=[1,1,2]
let v = uu6.range(0,12)
console.log('v=%j', v)

let t = T.new({r:v, shape:shape})
console.log('t.get(%j)=', idx, T.get(t, ...idx))
console.log('t=%s', T.str(t))

T.reshape(t, [3,4])

console.log('t=%s', T.str(t))

let nd = T.tensor2ndarray(t)
console.log('nd=%j', nd)

let t2 = T.ndarray2tensor(nd)
console.log('t2=%j', t2)

let t3 = T.add(t, t)
console.log('t3=%j', t3)

let t4 = T.sub(t, t)
console.log('t4=%j', t4)



