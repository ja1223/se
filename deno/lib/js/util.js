export function near(a,b,delta=0.001) {
  return (Math.abs(a-b) < delta)
}

export function clone(m) {
  return JSON.parse(JSON.stringify(m))
}
