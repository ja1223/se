export function vector(n, value=0) {
  let a = new Array(n)
  return a.fill(value)
}

export function near(a, b, delta=0.001) {
  if (a.length != b.length) return false
  let len = a.length
  for (var i = 0; i < len; i++) {
    if (Math.abs(a[i]-b[i]) > delta) return false
  }
  return true
}

export function dot(a, b) {
  let sum = 0
  let len = a.length
  for (var i = 0; i < len; i++) {
    sum += a[i] * b[i] // 速度較快
  }
  return sum
}

export function add(a, b) {
  let len = a.length
  let r = new Array(len)
  for (var i = 0; i < len; i++) {
    r[i] = a[i] + b[i]
  }
  return r
}

export function sub(a, b) {
  let len = a.length
  let r = new Array(len)
  for (var i = 0; i < len; i++) {
    r[i] = a[i] - b[i]
  }
  return r
}

export function mul(a, b) {
  let len = a.length
  let r = new Array(len)
  for (var i = 0; i < len; i++) {
    r[i] = a[i] * b[i]
  }
  return r
}

export function div(a, b) {
  let len = a.length
  let r = new Array(len)
  for (var i = 0; i < len; i++) {
    r[i] = a[i] / b[i]
  }
  return r
}

export function addc(a, c) {
  let len = a.length
  let r = new Array(len)
  for (var i = 0; i < len; i++) {
    r[i] = a[i] + c
  }
  return r
}

export function subc(a, c) {
  return addc(a, -c)
}

export function mulc(a, c) {
  let len = a.length
  let r = new Array(len)
  for (var i = 0; i < len; i++) {
    r[i] = a[i] * c
  }
  return r
}

export function divc(a, c) {
  return mulc(a, 1/c)
}