# web assembly

* https://www.assemblyscript.org/
    * 將 typescript 轉為 wasm

* [Why using WebAssembly and Rust together improves Node.js performance](https://developer.ibm.com/technologies/web-development/articles/why-webassembly-and-rust-together-improve-nodejs-performance/)
* [To WASM or not to WASM?](https://dev.to/linkuriousdev/to-wasm-or-not-to-wasm-3803)

