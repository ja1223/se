import ky from 'https://unpkg.com/ky/index.js';

(async () => {
    // const parsed = await ky.post('https://example.com', {json: {foo: true}}).json();
    var res = await ky.get('https://example.com');
    console.log('res=', res)

    res = await ky.post('https://example.com');
    console.log('res=', res)

    // console.log(parsed);
    //=> `{data: '🦄'}`
})();