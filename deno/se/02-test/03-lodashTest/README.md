# 練習 -- 模仿 lodash 套件實作

## 執行範例

```
PS D:\ccc\ccc109a\se\deno\se\02-test\03-lodashTest> deno run example/ex1.ts
_.chunk(['a', 'b', 'c', 'd'], 2)= [ [ "a", "b" ], [ "c", "d" ] ]
_.chunk(['a', 'b', 'c', 'd'], 3)= [ [ "a", "b", "c" ], [ "d" ] ]
```

## 單元測試

```
```

