# TonyQ

* [笑談軟體測試的幾個階段（一）](https://www.ptt.cc/bbs/Soft_Job/M.1332567899.A.C00.html)
* [笑談軟體測試的幾個階段（二）](https://www.ptt.cc/bbs/Soft_Job/M.1332601969.A.342.html)
* [笑談軟體測試的幾個階段（三）](https://www.ptt.cc/bbs/Soft_Job/M.1332681709.A.EB4.html)
* [笑談軟體測試的幾個階段（四）](https://www.ptt.cc/bbs/Soft_Job/M.1332697283.A.630.html)
* [笑談軟體測試的幾個階段（五）測試權重](https://www.ptt.cc/bbs/Soft_Job/M.1332863702.A.076.html)
* [笑談軟體測試的幾個階段（六）歷史代碼](https://www.ptt.cc/bbs/Soft_Job/M.1597938487.A.566.html)
